<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\PresidentController;
use App\Http\Controllers\ViceController;
use App\Http\Controllers\SecretaryController;
use App\Http\Controllers\TreasurerController;
use App\Http\Controllers\AuditorController;
use App\Http\Controllers\AddCandidateController;
use App\Http\Controllers\EditCandidateController;
use App\Http\Controllers\AddVoteController;
use App\Http\Controllers\ResultController;
use App\Http\Controllers\UpdateTitleController;
use App\Http\Controllers\UpdateKeyController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::resource('/checksenator', CheckSenatorController::class);
Route::resource('/getadmin', AdminController::class)->only([
    'index', 'show'
]);
Route::resource('/unlockadmin', AdminController::class)->only([
    'store', 'post'
]);
Route::resource('/updatetitle', UpdateTitleController::class)->only([
    'store', 'post'
]);
Route::resource('/updatekey', UpdateKeyController::class)->only([
    'store', 'post'
]);
Route::resource('/electionresult', ResultController::class)->only([
    'index', 'show'
]);
Route::resource('/president', PresidentController::class)->only([
    'index', 'show'
]);
Route::resource('/vice', ViceController::class)->only([
    'index', 'show'
]);
Route::resource('/secretary', SecretaryController::class)->only([
    'index', 'show'
]);
Route::resource('/treasurer', TreasurerController::class)->only([
    'index', 'show'
]);
Route::resource('/auditor', AuditorController::class)->only([
    'index', 'show'
]);
Route::resource('/addcandidate', AddCandidateController::class)->only([
    'store', 'post'
]);
Route::resource('/editcandidate', EditCandidateController::class)->only([
    'store', 'post'
]);
Route::delete('deletecandidate/{id}', 'App\Http\Controllers\EditCandidateController@destroy');

Route::resource('/addvote', AddVoteController::class)->only([
    'store', 'post'
]);
