<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Candidates;
class EditCandidateController extends Controller
{
    public function store(Request $request)
    {
        $candata = Candidates::find($request->id);
        $candata->fname = $request->input('fname');
        $candata->lname = $request->input('lname');
        $candata->mi = $request->input('mi');
        $candata->position = $request->input('position');
        $candata->save();
        return response()->json($candata);
    }
    public function destroy($id)
    {
        $candata = Candidates::find($id);
        $candata->delete();
        return response()->json($candata);
    }
}
