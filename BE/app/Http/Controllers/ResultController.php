<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Candidates;

class ResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $presname = "";
        $presvote = 0;
        $candidate = Candidates::where('position','=','President')->get();
        $i = count($candidate);
        $j = 0;
        while($j < $i){
            if($candidate[$j]->votes > $presvote){
                $presvote = $candidate[$j]->votes;
                $presname = $candidate[$j]->lname.', '.$candidate[$j]->fname.' '.$candidate[$j]->mi;
            }elseif($candidate[$j]->votes == $presvote && $candidate[$j]->votes != 0){
                $presname .= ' & '.$candidate[$j]->lname.', '.$candidate[$j]->fname.' '.$candidate[$j]->mi;
            }
            $j++;
        }
        $presdata = array($presname, $presvote);

        /** */
        $vpresname = "";
        $vpresvote = 0;
        $candidate = Candidates::where('position','=','Vice-President')->get();
        $i = count($candidate);
        $j = 0;
        while($j < $i){
            if($candidate[$j]->votes > $vpresvote){
                $vpresvote = $candidate[$j]->votes;
                $vpresname = $candidate[$j]->lname.', '.$candidate[$j]->fname.' '.$candidate[$j]->mi;
            }elseif($candidate[$j]->votes == $vpresvote && $candidate[$j]->votes != 0){
                $vpresname .= ' & '.$candidate[$j]->lname.', '.$candidate[$j]->fname.' '.$candidate[$j]->mi;
            }
            $j++;
        }
        $vpresdata = array($vpresname, $vpresvote);
        /** */
        $secname = "";
        $secvote = 0;
        $candidate = Candidates::where('position','=','Secretary')->get();
        $i = count($candidate);
        $j = 0;
        while($j < $i){
            if($candidate[$j]->votes > $secvote){
                $secvote = $candidate[$j]->votes;
                $secname = $candidate[$j]->lname.', '.$candidate[$j]->fname.' '.$candidate[$j]->mi;
            }elseif($candidate[$j]->votes == $secvote && $candidate[$j]->votes != 0){
                $secname .= ' & '.$candidate[$j]->lname.', '.$candidate[$j]->fname.' '.$candidate[$j]->mi;
            }
            $j++;
        }
        $secdata = array($secname, $secvote);
        /** */
        $treasname = "";
        $treasvote = 0;
        $candidate = Candidates::where('position','=','Treasurer')->get();
        $i = count($candidate);
        $j = 0;
        while($j < $i){
            if($candidate[$j]->votes > $treasvote){
                $treasvote = $candidate[$j]->votes;
                $treasname = $candidate[$j]->lname.', '.$candidate[$j]->fname.' '.$candidate[$j]->mi;
            }elseif($candidate[$j]->votes == $treasvote && $candidate[$j]->votes != 0){
                $treasname .= ' & '.$candidate[$j]->lname.', '.$candidate[$j]->fname.' '.$candidate[$j]->mi;
            }
            $j++;
        }
        $treasdata = array($treasname, $treasvote);
        /** */
        $audname = "";
        $audvote = 0;
        $candidate = Candidates::where('position','=','Auditor')->get();
        $i = count($candidate);
        $j = 0;
        while($j < $i){
            if($candidate[$j]->votes > $audvote){
                $audvote = $candidate[$j]->votes;
                $audname = $candidate[$j]->lname.', '.$candidate[$j]->fname.' '.$candidate[$j]->mi;
            }elseif($candidate[$j]->votes == $audvote && $candidate[$j]->votes != 0){
                $audname .= ' & '.$candidate[$j]->lname.', '.$candidate[$j]->fname.' '.$candidate[$j]->mi;
            }
            $j++;
        }
        $auddata = array($audname, $audvote);

        $candidatedata = array($presdata, $vpresdata, $secdata, $treasdata, $auddata);
        return response()->json($candidatedata);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
