<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Voters;
use App\Models\Candidates;

class AddVoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $addvote = new Voters;
        $addvote->vid = $request->input("vid");
        $addvote->save();

        $addvote = Candidates::find($request->input("presid"));
        $addvote->votes = $addvote->votes + 1;
        $addvote->save();

        $addvote = Candidates::find($request->input("vpresid"));
        $addvote->votes = $addvote->votes + 1;
        $addvote->save();

        $addvote = Candidates::find($request->input("secid"));
        $addvote->votes = $addvote->votes + 1;
        $addvote->save();

        $addvote = Candidates::find($request->input("treasid"));
        $addvote->votes = $addvote->votes + 1;
        $addvote->save();

        $addvote = Candidates::find($request->input("audid"));
        $addvote->votes = $addvote->votes + 1;
        $addvote->save();
        return response()->json($addvote);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
