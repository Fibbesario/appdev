import Vuex from 'vuex';
import Vue from 'vue';
import axios from 'axios';

Vue.use(Vuex, axios);

export default new Vuex.Store({
    state: {
        admindata: [],
        electionresult: [],
        adminkeytemp: '',
        presidents: [],
        presnum: 0,
        vices: [],
        secretarys: [],
        treasurers: [],
        auditors: [],
        president: {
            id: '',
            name: '',
            votes: ''
        },
        input: {
            fname: '',
            lname: '',
            mi: '',
            position: '',
        }
    },
    getters: {
        //
    },
    actions: {
        unlockadmin({ commit }) {
            axios.post('/api/unlockadmin', {
                status: "unlock",
                completed: false
            }).then((response) => {
                console.log(response)
                commit('setAdmin', response.data)
            })
        },
        lockadmin({ commit }) {
            axios.post('/api/unlockadmin', {
                status: "lock",
                completed: false
            }).then((response) => {
                console.log(response)
                commit('setAdmin', response.data)
            })
        },
        updateTitle({ commit }, data) {
            axios.post('/api/updatetitle', {
                electionname: data,
                completed: false
            }).then((response) => {
                console.log(response)
                commit('setAdmin', response.data)
            })
        },
        updateKey({ commit }, data) {
            axios.post('/api/updatekey', {
                adminkey: data,
                completed: false
            }).then((response) => {
                commit('setAdmin', response.data)
            })
        },
        fetchAdmin({ commit }) {
            axios.get('/api/getadmin').then((response) => {
                commit('setAdmin', response.data)
            })
        },
        fetchResult({ commit }) {
            axios.get('/api/electionresult').then((response) => {
                commit('setResult', response.data)
            })
        },
        fetchPresident({ commit }) {
            axios.get('/api/president').then((response) => {
                this.presidents = response.data
                console.log(response.data)
                commit('setPresidents', response.data)
            })
        },
        fetchVice({ commit }) {
            axios.get('/api/vice').then((response) => {
                this.vices = response.data
                console.log(response.data)
                commit('setVices', response.data)
            })
        },
        fetchSecretary({ commit }) {
            axios.get('/api/secretary').then((response) => {
                this.secretarys = response.data
                console.log(response.data)
                commit('setSecretarys', response.data)
            })
        },
        fetchTreasurer({ commit }) {
            axios.get('/api/treasurer').then((response) => {
                this.treasurers = response.data
                console.log(response.data)
                commit('setTreasurers', response.data)
            })
        },
        fetchAuditor({ commit }) {
            axios.get('/api/auditor').then((response) => {
                this.auditors = response.data
                console.log(response.data)
                commit('setAuditors', response.data)
            })
        },
        addCandidate({ commit }, input) {
            axios.post('/api/addcandidate', {
                fname: input.fname,
                lname: input.lname,
                mi: input.mi,
                position: input.position,
                completed: false
            }).then((response) => {
                commit('newCandidates', response.data)
            })
        },
        editCandidate({ commit }, input) {
            axios.post('/api/editcandidate', {
                id: input.id,
                fname: input.fname,
                lname: input.lname,
                mi: input.mi,
                position: input.position,
                completed: false
            }).then((response) => {
                commit('newCandidates', response.data)
            })
        },
        deleteCandidate({ commit }, input) {
            axios.delete('/api/deletecandidate/'+input.id)
            commit('newCandidates', input.id)
        },
        addVote({ commit }, input) {
            axios.post('/api/addvote', {
                vid: input.vid,
                presid: input.presid,
                vpresid: input.vpresid,
                secid: input.secid,
                treasid: input.treasid,
                audid: input.audid,
                completed: false
            }).then((response) => {
                commit('newVote', response.data)
            })
        },
    },
    mutations: {
        setAdmin: (state, admin) => {
            state.adminkeytemp = admin[0].adminkey;
            state.admindata = admin;
        },
        setResult: (state, result) => {
            state.electionresult = result;
        },
        setPresidents: (state, president) => {
            state.presidents = president;
        },
        setVices: (state, vice) => {
            state.vices = vice;
        },
        setSecretarys: (state, secretary) => {
            state.secretarys = secretary;
        },
        setTreasurers: (state, treasurer) => {
            state.treasurers = treasurer;
        },
        setAuditors: (state, auditor) => {
            state.auditors = auditor;
        },
        newCandidates: (state, data) => {
            console.log(data);
        },
        newVote: (data) => {
            console.log(data);
        },
    },

});