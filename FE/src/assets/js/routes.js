import Vue from 'vue'
import VueRouter from 'vue-router'

//import Dash from '../../components/pages/admin/Dash'
import Candidates from '../../components/pages/admin/Candidates'
import AddCandidate from '../../components/pages/admin/AddCandidates'
import ElectionDetails from '../../components/pages/admin/ElectionDetails'
import Balot from '../../components/pages/voter/Balot'

//for router
Vue.use(VueRouter)

export const router = new VueRouter({
    routes: [
        { path: '/', component: Candidates },
        { path: '/candidates', component: Candidates },
        { path: '/addcandidate', component: AddCandidate },
        { path: '/electiondetails', component: ElectionDetails },
        { path: '/balot', component: Balot },
    ]
  })
  